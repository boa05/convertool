var options = ["mm", "cm", "dm", "m"];
var values = [0.001, 0.01, 0.1, 1];

var types = ["hosszúság", "tömeg"]

var roundToDecs = 2;

var selectedOption = 0;
var selectedOption2 = 0;
var selectedType = 0;

var dropBtn = document.getElementById("dropBtn");
var dropBtn2 = document.getElementById("dropBtn2");
var typeBtn = document.getElementById("typeBtn");

var dropPanel = document.getElementById("drop");
var dropPanel2 = document.getElementById("drop2");
var typePanel = document.getElementById("typeDrop");

var dropen = false;

function multiplyDecs() {
  return Math.pow(10, roundToDecs);
}

function selectDrop(n) {
  selectedOption = n;
  dropBtn.innerHTML = options[selectedOption];
  hideDrop();
}

function selectDrop2(n) {
  selectedOption2 = n;
  dropBtn2.innerHTML = options[selectedOption2];
  hideDrop2();
}

function selectType(n) {
  selectedType = n;
  updateSelectedType();
  typeBtn.innerHTML = types[n];
  dropBtn.innerHTML = options[selectedOption];
  dropBtn2.innerHTML = options[selectedOption2];
  hideType();
}

function showDrop() {
  if (!dropen) {
    hideDrop();
    for (var n = 0; n < options.length; n++) {
      var element = document.createElement("button");
      var text = document.createTextNode(options[n]);
      element.appendChild(text);
      element.className = "dropz";
      element.onclick = function() {
        var i = Array.from(this.parentNode.children).indexOf(this);
        selectDrop(i);
      }
      dropPanel.appendChild(element);
      dropen = true;
    }
  } else {
    hideDrop();
  }
}

function showDrop2() {
  if (!dropen) {
    hideDrop2();
    for (var n = 0; n < options.length; n++) {
      var element = document.createElement("button");
      var text = document.createTextNode(options[n]);
      element.appendChild(text);
      element.className = "dropz";
      element.onclick = function() {
        var i = Array.from(this.parentNode.children).indexOf(this);
        selectDrop2(i);
      }
      dropPanel2.appendChild(element);
      dropen = true;
    }
  } else {
    hideDrop2();
  }
}

function showType() {
  if (!dropen) {
    hideType();
    for (var n = 0; n < types.length; n++) {
      var element = document.createElement("button");
      var text = document.createTextNode(types[n]);
      element.appendChild(text);
      element.className = "dropz";
      element.onclick = function() {
        var i = Array.from(this.parentNode.children).indexOf(this);
        selectType(i);
      }
      typePanel.appendChild(element);
      dropen = true;
    }
  } else {
    hideType();
  }
}

function hideDrop() {
  while (dropPanel.firstChild) {
    dropPanel.removeChild(dropPanel.firstChild);
  }
  dropen = false;
}

function hideDrop2() {
  while (dropPanel2.firstChild) {
    dropPanel2.removeChild(dropPanel2.firstChild);
  }
  dropen = false;
}

function hideType() {
  while (typeDrop.firstChild) {
    typeDrop.removeChild(typeDrop.firstChild);
  }
  dropen = false;
}

var result = document.getElementById("result");
var cvtFromTx = document.getElementById("cvtFromTx");

function calculate() {
  result.innerHTML = Math.round(((values[selectedOption] / values[selectedOption2]) * cvtFromTx.value) * multiplyDecs()) / multiplyDecs();
}

function updateSelectedType(){
  switch (selectedType) {
    case 0://hosszúság
      options = ["mm", "cm", "dm", "m"];
      values = [0.001, 0.01, 0.1, 1];
      break;
    case 1://tömeg
      options = ["g", "dkg", "kg", "t"];
      values = [0.001, 0.01, 1, 1000];
      break;

    default:

  }
}

window.addEventListener('mouseup', function(event) {
  if (event.target != dropPanel && event.target != dropPanel2 &&
     event.target != typePanel && event.target.parentNode != dropPanel &&
     event.target.parentNode != dropPanel2 && event.target.parentNode != typePanel &&
     dropen && event.target != dropBtn && event.target != dropBtn2 && event.target != typeBtn) {
    hideDrop();
    hideDrop2();
    hideType();
  }
});
